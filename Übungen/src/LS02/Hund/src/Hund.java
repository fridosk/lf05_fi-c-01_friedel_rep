public class Hund {
    int alter;
    String name;

    public void kunststueck(){
        System.out.println(this.name + " macht ein Kunststück");
    }

    public Hund(){
        this.alter = 0;
        this.name = "Jack";
    }

    public Hund(int alter, String name){
        this.alter = alter;
        this.name = name;
    }

    public int getAlter() {
        return alter;
    }

    public String getName() {
        return name;
    }

    public void setAlter(int alter) {
        this.alter = alter;
    }

    public void setName(String name) {
        this.name = name;
    }
}
