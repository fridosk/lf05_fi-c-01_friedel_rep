
public class TestMain {
    public static void main(String[] args) {
        Hund Jack = new Hund();
        Hund Haso = new Hund(3, "Haso");

        System.out.println("Mein Hund heißt: "+Haso.getName()+" und ist "+ Haso.getAlter()+" Jahre alt");
        System.out.println("Mein Hund heißt: "+Jack.getName()+" und ist "+ Jack.getAlter()+" Jahre alt");

        Jack.kunststueck();

    }
}
