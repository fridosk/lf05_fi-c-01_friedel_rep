import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Test123 {
	
    static double zuZahlenderBetrag; 
    static double eingeworfeneMuenze;
    static double rueckgabebetrag;
    static double einzelpreis; 
    static byte ticketAnzahl;
    //-------------------------------------//
    
    public static double fahrkartenbestellungErfassen(){
    	
    	Scanner tastatur = new Scanner(System.in);
	    System.out.print("Zu zahlender Betrag (EURO): ");
	    einzelpreis = tastatur.nextDouble();
	    System.out.print("Anzahl der Tickets: ");
	    ticketAnzahl = tastatur.nextByte();
	    zuZahlenderBetrag = (ticketAnzahl * einzelpreis);
	    
	    return zuZahlenderBetrag;
    }
    //-------------------------------------------------//
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag){
    	
    	if (zuZahlenderBetrag == 0) {
    		System.out.println("Keine Zahlung erforderlich");
    		main(null);
    	}
       
        
        Scanner tastatur = new Scanner(System.in);
 	  
 	   double eingezahlterGesamtbetrag = 0;
 	   while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
 	   		System.out.printf("Noch zu Zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
 	   		System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
 	   		eingeworfeneMuenze = tastatur.nextDouble();
 	   		eingezahlterGesamtbetrag += eingeworfeneMuenze;
 	   }
 	   double restbetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
 	   return restbetrag;
    }
    
   //----------------------------------------------------------------------//
    
    public static void fahrkartenAusgeben(){
    	
 	   	System.out.println("\nFahrschein wird ausgegeben");
 	   	for (int i = 0; i < 8; i++)
 	   	{
 	   		System.out.print("=");
 	   		try {
 	   			Thread.sleep(250);
 	   		} catch (InterruptedException e) {
 			   e.printStackTrace();
 	   			}
 	   	}
    }
	//----------------------------------------------------//
    
    public static void rueckgeldAusgeben(double rueckgabebetrag) {
	       if(rueckgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der Rueckgabebetrag in Hoehe von %.2f Euro wird in folgenden Muenzen ausgezahlt:\n", rueckgabebetrag);
	
	           while(rueckgabebetrag > 1.99) // 2 EURO-Muenzen
	           {
	        	  System.out.println("2 EURO");
		          rueckgabebetrag -= 2.0;
	           }
	           while(rueckgabebetrag > 0.99) // 1 EURO-Muenzen
	           {
	        	  System.out.println("1 EURO");
		          rueckgabebetrag -= 1.0;
	           }
	           while(rueckgabebetrag > 0.49) // 50 CENT-Muenzen
	           {
	        	  System.out.println("50 CENT"); 
		          rueckgabebetrag -= 0.5;
	           }
	           while(rueckgabebetrag > 0.19) // 20 CENT-Muenzen
	           {
	        	  System.out.println("20 CENT");
	 	          rueckgabebetrag -= 0.2;
	           }
	           while(rueckgabebetrag > 0.09) // 10 CENT-Muenzen
	           {
	        	  System.out.println("10 CENT");
		          rueckgabebetrag -= 0.1;
	           }
	           while(rueckgabebetrag > 0.04)// 5 CENT-Muenzen
	           {
	        	  System.out.println("5 CENT");
	 	          rueckgabebetrag -= 0.05;
	           }
	       }
    	}
    	//-----------------------------------------------------------//

	public static void main(String[] args) {
		
		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		System.out.println("\n\n");
		rueckgeldAusgeben(rueckgabebetrag);
	
	}

}
