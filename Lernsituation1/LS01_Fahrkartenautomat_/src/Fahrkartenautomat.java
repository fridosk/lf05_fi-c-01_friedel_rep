import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Fahrkartenautomat {



	public static double ticketpreisErfassen() {

		String[] bezeichnungen =  {"|Einzelfahrschein Berlin AB 		|",
				"|Einzelfahrschein Berlin BC 		|",
				"|Einzelfahrschein Berlin ABC		|",
				"|Kurzstrecke                		|",
				"|Tageskarte Berlin AB       		|",
				"|Tageskarte Berlin BC       		|",
				"|Tageskarte Berlin ABC      		|",
				"|Kleingruppen-Tageskarte Berlin AB	|",
				"|Kleingruppen-Tageskarte Berlin BC	|",
				"|Kleingruppen-Tageskarte Berlin ABC   |"};

		double[] ticketpreise = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};

		byte ticketAnzahl = 0;
		double zuZahlenderBetrag = 0.0;
		double einzelpreis = 0.0;

		Scanner tastatur = new Scanner(System.in);

		while (true) {
			System.out.printf("\nW�hlen Sie ihre Wunschfahrkarte aus:\n\n");
			int i=0;
			for (; i<bezeichnungen.length; i++)
				System.out.printf("  %s [%.2f EUR] [%d]\n\n", bezeichnungen[i], ticketpreise[i], i+1);
			System.out.printf("  |Bezahlen			        "
					+ "| (%d) \n\n\n  [%.2f EUR]\n", i+1, zuZahlenderBetrag);
			System.out.print("\n\nIhre Wahl: ");
			try {
				ticketAnzahl = tastatur.nextByte();
			}
			catch(InputMismatchException e) {
				ticketAnzahl = 0;
				tastatur.nextLine();
			}
			try {
				einzelpreis = ticketpreise[ticketAnzahl-1];
			}
			catch (ArrayIndexOutOfBoundsException exception) {
				if (ticketAnzahl == i+1) {
					break;
				}
				else {System.out.println(" >>falsche Eingabe<<"); warte(500);continue;}
			}
			zuZahlenderBetrag += ticketanzahlErfassen(einzelpreis);
		}
		return zuZahlenderBetrag;



	}
	//-----------------------------------------

	public static double ticketanzahlErfassen(double einzelpreis){

		Scanner tastatur = new Scanner(System.in);

		byte ticketAnzahl = 0;

		while (true) {
			System.out.print("Anzahl der Tickets: ");
			try {
				ticketAnzahl = tastatur.nextByte();
			}
			catch(InputMismatchException e) {
				ticketAnzahl = 0;
				tastatur.nextLine();
			}

			if (ticketAnzahl < 0 || ticketAnzahl >= 10)
			{
				System.out.println("\nTicket-Anzahl darf nur zwischen 1-10 liegen...\n\nBitte versuche Sie es erneut.\n");
				continue;
			}
			break;
		}

		return ticketAnzahl * einzelpreis;
	}
	//-------------------------------------------------

	public static double fahrkartenBezahlen(double zuZahlenderBetrag){

		if (zuZahlenderBetrag == 0) {
			System.out.println("Keine Zahlung erforderlich"); warte(500);
			main(null);
		}


		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0;
		double eingeworfeneMuenze;

		while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu Zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
			eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		double restbetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return restbetrag;
	}
	//----------------------------------------------------------------------

	public static void fahrkartenAusgeben(){

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++)
		{
			System.out.print("=");
			warte(100);
		}
		System.out.print("\n\n");
		System.out.println("\n\n");
	}
	//----------------------------------------------------

	public static void rueckgeldAusgeben(double rueckgabebetrag) {

		if(rueckgabebetrag > 0.0)
		{
			System.out.printf("Der Rueckgabebetrag in Hoehe von %.2f Euro wird in folgenden Muenzen ausgezahlt:\n\n", rueckgabebetrag);

			while(rueckgabebetrag > 1.99) // 2 EURO-Muenzen
			{
				muenzeAusgeben(2, "EURO");
				rueckgabebetrag -= 2.0;
			}
			while(rueckgabebetrag > 0.99) // 1 EURO-Muenzen
			{
				muenzeAusgeben(1, "EURO");
				rueckgabebetrag -= 1.0;
			}
			while(rueckgabebetrag > 0.49) // 50 CENT-Muenzen
			{
				muenzeAusgeben(50, "CENT");
				rueckgabebetrag -= 0.5;
			}
			while(rueckgabebetrag > 0.19) // 20 CENT-Muenzen
			{
				muenzeAusgeben(20, "CENT");
				rueckgabebetrag -= 0.2;
			}
			while(rueckgabebetrag > 0.09) // 10 CENT-Muenzen
			{
				muenzeAusgeben(10, "CENT");
				rueckgabebetrag -= 0.1;
			}
			while(rueckgabebetrag > 0.04)// 5 CENT-Muenzen
			{
				muenzeAusgeben(5, "CENT");
				rueckgabebetrag -= 0.05;
			}
			System.out.println("\n\n");
		}
	}
	//-----------------------------------------------------------

	public static void kaufvorgangBeenden() {

		Scanner tastatur = new Scanner(System.in);

		System.out.println("Vergessen Sie nicht, den Fahrschein\n"
				+ "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen eine angenehme Fahrt!\n");
		warte(6000);
	}
	//--------------------------------------

	public static void warte(int ms)
	{
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//------------------------------

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}
	//-------------------------------------------------------------

	public static void main(String[] args) {


		double rueckgabebetrag;
		double zuZahlenderBetrag;

		while(true) {

			//ticketanzahlErfassen-Aufruf innerhalb ticketpreisErfassen
			zuZahlenderBetrag = ticketpreisErfassen();
			rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(rueckgabebetrag);
			kaufvorgangBeenden();

		}
	}

}
