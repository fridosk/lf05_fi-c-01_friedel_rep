package StarTrek;

public class Testumgebung {
    public static void main(String[] args) {
        Raumschiff klingonen = new Raumschiff(1, 100, 100,100,
                100, 2,"IKS Hegh'ta");
        Raumschiff romulaner = new Raumschiff(2,100,100,100,100,2,"IRW Khazara");
        Raumschiff vulkanier = new Raumschiff(0,80,80,50,100,5,"Ni'Var");

        Ladung ladungK1 = new Ladung("Ferengi Schneckensaft", 200);
        Ladung ladungK2 = new Ladung("Bat'leth Klingonen Schwert", 200);

        Ladung ladungR1 = new Ladung("Borg-Schrott", 5);
        Ladung ladungR2 = new Ladung("Rote Materie", 2);
        Ladung ladungR3 = new Ladung("PLasma-Waffe", 50);

        Ladung ladungV1 = new Ladung("Forschungssonde", 35);
        Ladung ladungV2 = new Ladung("Photonentorpedo", 3);


        klingonen.addLadung(ladungK1);
        klingonen.addLadung(ladungK2);

        romulaner.addLadung(ladungR1);
        romulaner.addLadung(ladungR2);
        romulaner.addLadung(ladungR3);

        vulkanier.addLadung(ladungV1);
        vulkanier.addLadung(ladungV2);


        klingonen.photonentorpedosLaden(1);klingonen.photonentorpedoSchießen(romulaner);
        klingonen.zustandRaumschiff();

        romulaner.phaserkanoneSchießen(klingonen);
        klingonen.zustandRaumschiff();

        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch!");

        klingonen.zustandRaumschiff();klingonen.ladungsverzeichnisAusgeben();

        vulkanier.reparaturDurchfuehren(true,true,true,5);
        vulkanier.addLadung(ladungV2);vulkanier.ladungsverzeichnisAufrauemen();

        klingonen.photonentorpedosLaden(2);
        klingonen.photonentorpedoSchießen(romulaner);

        klingonen.zustandRaumschiff();klingonen.ladungsverzeichnisAusgeben();
        romulaner.zustandRaumschiff();romulaner.ladungsverzeichnisAusgeben();
        vulkanier.zustandRaumschiff();vulkanier.ladungsverzeichnisAusgeben();





    }
}
