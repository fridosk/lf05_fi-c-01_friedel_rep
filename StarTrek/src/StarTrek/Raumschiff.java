/**
 * This class implements a spaceship with several parameters which can interact with other spaceship objects.
 *
 * This code was written by order of the Consulting GmbH.
 *
 * @author Oskar Friedel
 * @version 0.1
 * @since 16.04.2021
 */

package StarTrek;

/**
 * Import from required libaries
 */

//An instance of this class is used to generate a stream of pseudorandom numbers.
import java.util.Collections;
import java.util.Random;
//Implements the needed function for Arraylists
import java.util.ArrayList;


/**
 * Declaration of various parameters
 */
public class Raumschiff {
    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhlatungssystemeInProzent;
    private int androidenAnzahl;
    private String schiffsname;
    private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
    private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
    public int anzahlTorpedos;
    private String message;
    public String click = "\n"+"-=*Click*=-";
    //

    /**
     * Implements getter & setter Methods
     *
     * Getter:
     * @return the named parameter
     *
     * Setter:
     * Overwrites the committed value and loads it into the named parameter
     */

    public int getPhotonentorpedoAnzahl() {
        return photonentorpedoAnzahl;
    }
    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
    }

    public int getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }
    public void setEnergieversorgungInProzent(int zustandenergieversorgungInProzentNeu) {
        this.energieversorgungInProzent = zustandenergieversorgungInProzentNeu;
    }

    public int getSchildeInProzent() {
        return schildeInProzent;
    }
    public void setSchildeInProzent(int zustandschildeInProzentNeu) {
        this.schildeInProzent = zustandschildeInProzentNeu;
    }

    public int getHuelleInProzent() {
        return huelleInProzent;
    }
    public void setHuelleInProzent ( int huelleInProzentNeu) {
        this.huelleInProzent = huelleInProzentNeu;
    }

    public int getLebenserhlatungssystemeInProzent () {
        return lebenserhlatungssystemeInProzent;
    }
    public void setLebenserhlatungssystemeInProzent (int lebenserhlatungssystemeInProzentNeu) {
        this.lebenserhlatungssystemeInProzent = lebenserhlatungssystemeInProzentNeu;
    }

    public int getAndroidenAnzahl(){
        return androidenAnzahl;
    }
    public void setAndroidenAnzahl(int androidenAnzahl){
        this.androidenAnzahl = androidenAnzahl;
    }

    public String getSchiffsname() {
        return schiffsname;
    }
    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }


    //-----------------------------------------------------------------------------------------------------------------

    // Anlegung eines Raumschiff-Objekts mit diveresen Parametern

    /**
     * Creates a spaceship object with
     * @param photonentorpedoAnzahl
     * @param energieversorgungInProzent
     * @param zustandSchildeInProzent
     * @param zustandHuelleInProzent
     * @param zustandLebenserhaltungssystemeInProzent
     * @param anzahlDroiden
     * @param schiffsname
     * as parameters and loads them
     */
    public Raumschiff (int photonentorpedoAnzahl, int energieversorgungInProzent,
                       int zustandSchildeInProzent, int zustandHuelleInProzent,
                       int zustandLebenserhaltungssystemeInProzent,int anzahlDroiden,String schiffsname){
        // Überladung der zugewiesenen Variablenwerte in die bereits deklarierte Variablen
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.schildeInProzent = zustandSchildeInProzent;
        this.huelleInProzent = zustandHuelleInProzent;
        this.lebenserhlatungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
        this.androidenAnzahl = anzahlDroiden;
        this.schiffsname = schiffsname;
    }

//-----------------------------------------------------------------------------------------------------------------

    /**
     * Adds a "load" from a spaceship to an Arraylist @ladungsverzeichnis
     * @param neueLadung
     */
    public void addLadung(Ladung neueLadung) {
        /*Requirement: Load has to be a "Photonentorpedo"
        Sets the photonentorpedoAnzahl to the amount
        Sets "Photonentorpedo.Menge" to zero
         */
        if (neueLadung.getBezeichnung() == "Photonentorpedo") {
            setPhotonentorpedoAnzahl(neueLadung.getMenge() + this.getPhotonentorpedoAnzahl());
            ladungsverzeichnis.add(neueLadung);
            neueLadung.setMenge(0);
            System.out.println(neueLadung.getBezeichnung() + " hinzugefügt!\n");
        }
        //Adds the any other load to the Arraylist
        else{
            ladungsverzeichnis.add(neueLadung);
            System.out.println(neueLadung.getBezeichnung() + " hinzugefügt!\n");
        }
    }


    /**
     * Simulates a hit of the spacehip and negates the parameters of @param r
     * @param r is the spaceship to be shot at
     */
    private void treffer(Raumschiff r) {
        System.out.println("\nTreffer!");

        r.setSchildeInProzent(r.getSchildeInProzent()-50);

        if (r.getSchildeInProzent()<= 0) {
            System.out.println("\nSchild zerstört!!");
            r.setHuelleInProzent(r.getHuelleInProzent()-50);             //Abbau Huelle
            r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent()-50);         //Abbau Energieversorgung
            if (r.getHuelleInProzent() <= 0) {
                r.setLebenserhlatungssystemeInProzent(0);
                System.out.println("\nHuelle zerstört!!");
                System.out.println("\nLebenserhaltungssystem sind vollständig vernichtet!");
            }
        }
    }

    //fikitver Phaserkanonenschuss der bei einem Treffer Methode: "treffer()" ausführt + Konsolenausgabe

    /**
     * Is a requirement for the @photonentorpedoSchießen method, loads the wanted photonentorpedoAnzahl so it can be shot
     * @param anzahlTorpedosNeu amount of desired Photonentorpedos
     */
    public void photonentorpedosLaden(int anzahlTorpedosNeu){
        anzahlTorpedos=anzahlTorpedosNeu;
        if (anzahlTorpedos==0)
            System.out.println("Es können nicht 0 Torpedos geladen werden");
        else
            System.out.println(anzahlTorpedos+" Photonentorpedos geladen");
    }

    /**
     * Shoots the loaded amount of Photonentorpedos and executes @treffer method
     * @param r enemy spaceship which should be shooted
     */
    //fikitver Photonentorpedoschuss der bei einem Treffer Methode: "treffer()" ausführt + Konsolenausgabe
    public void photonentorpedoSchießen(Raumschiff r) {

        String nameFeindlichesRaumschiff = r.getSchiffsname();
        String nameEigenesRaumschiff = this.getSchiffsname();

        //Ends the method if the Photonentorpedoanzahl = 0
        if (this.getPhotonentorpedoAnzahl() == 0) {
            System.out.println("[Abbruch]" + "Keine Photonentorpedos gefunden!");
            nachrichtAnAlle(click);
        }
        //Ends the method if the spaceship name is the same as the enemy spaceship
        else if (nameEigenesRaumschiff == nameFeindlichesRaumschiff) {
            System.out.println("Du kannst dich nicht selbst beschießen!");
        }
        //Negates @anzahlTorpedos if its less then @photonenTorpedoanzahl
        else if (anzahlTorpedos < this.getPhotonentorpedoAnzahl()) {
            nachrichtAnAlle(anzahlTorpedos + " Photonentorpedos eingesetzt");
            setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl()-anzahlTorpedos);
            treffer(r);
        }
        //Sets the @anzahlTorpedos = 0 if its more than @photonenTorpedoanzahl
        else if (anzahlTorpedos > this.getPhotonentorpedoAnzahl()) {
            nachrichtAnAlle(this.getPhotonentorpedoAnzahl() + " Photonentorpedos eingesetzt");
            setPhotonentorpedoAnzahl(0);
            treffer(r);
        }
        //Shoots the Photonentorpedo if all cases dont occur
        else {
            nachrichtAnAlle(this.getPhotonentorpedoAnzahl() + " Photonentorpedos eingesetzt");
            setPhotonentorpedoAnzahl(0);
            treffer(r);
        }
    }

    /**
     * Same procedure as @photonenTorpedoschießen with other cases
     * @param r enemy spaceship
     */

    public void phaserkanoneSchießen(Raumschiff r){
        String nameFeindlichesRaumschiff = r.getSchiffsname();
        String nameEigenesRaumschiff = this.getSchiffsname();

        if(nameEigenesRaumschiff == nameFeindlichesRaumschiff ){
            System.out.println("Du kannst dich nicht selbst beschießen!");
        }
        else {
            if (this.getEnergieversorgungInProzent() < 50)
                nachrichtAnAlle(click);
            else {
                setEnergieversorgungInProzent(this.getEnergieversorgungInProzent()-50);
                nachrichtAnAlle("Phaserkanone abgeschossen!");
                treffer(r);
            }
        }
    }


    /**
     * Adds a message to an Arraylist and puts it out
     * @param message
     */
    public void nachrichtAnAlle(String message){
        broadcastKommunikator.add(message);
        System.out.println(message+"\n");
    }

    /**
     * Prints out the collected messages from @nachrichtAnAlle
     */
    public void eintraegeLogbuchZurueckgeben() {

        int p = 0;
        int e = 1;
        while (p < broadcastKommunikator.size()) {
            System.out.println("Logbucheintrag " + e + ": " + broadcastKommunikator.get(p));
            p++;
            e++;
        }
    }

    /**
     * Simulates a repairment of the spaceship, changes given parameter to (random number + the existing value)
     *
     * Indicates which parameter should change
     * @param schutzschilde
     * @param energieversorgung
     * @param schiffshuelle
     *
     * @param anzahlDroiden
     */
    public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung,
                                      boolean schiffshuelle, int anzahlDroiden){

        //Method of the random libary which creates a random number beetween 0-100
        Random z = new Random();
        int zufallszahl = z.nextInt(100);


        int s = 0;
        int s1 = 0;
        int s2 = 0;
        int s3 = 0;

        //Increments a variable to indicate which spaceship structure should be repaired
        if(schutzschilde = true)
            s1++;
        if(energieversorgung = true)
            s2++;
        if(schiffshuelle = true){
            s3++;
        }

        else
            System.out.println("Keine Reperatur erforderlich!");

        s=s1+s2+s3;

        double berechnungZufall = (((zufallszahl) * anzahlDroiden)/s);
        double prozentBerechnung = (berechnungZufall);

        if (anzahlDroiden > this.getAndroidenAnzahl()) {
            nachrichtAnAlle("\n"+this.getAndroidenAnzahl() + " Reperaturdroiden wurden eingesetzt");
            setAndroidenAnzahl(0);
        }
        else{
            nachrichtAnAlle("\n"+this.getAndroidenAnzahl() + " Reperaturdroiden wurden eingesetzt");
        }

        //Sets the wanted spaceship structure to (given paramters + random number) - so that they are repaired
        if(s1 == 1){
            int t = this.getSchildeInProzent();
            int u = (((int)(prozentBerechnung)*t)/100)+this.getSchildeInProzent();
            if (u >= 100) u = 100;
            setSchildeInProzent(u);
        }
        if (s2 == 1){
            int t = this.getEnergieversorgungInProzent();
            int u = (((int)(prozentBerechnung)*t)/100)+this.getEnergieversorgungInProzent();
            if (u >= 100) u = 100;
            setEnergieversorgungInProzent(u);
        }
        if (s3 == 1){
            int t = this.getHuelleInProzent();
            int u = (((int)(prozentBerechnung)*t)/100)+this.getHuelleInProzent();
            if (u >= 100) u = 100;
            setHuelleInProzent(u);
        }
    }



    //Output of the current spaceship condition
    public void zustandRaumschiff(){
        System.out.println("-----------------------------------");
        System.out.println("Photonenanzahl:                "+getPhotonentorpedoAnzahl());
        System.out.println("Energieversorgung:             "+getEnergieversorgungInProzent()+"%");
        System.out.println("Schildzustand:                 "+getSchildeInProzent()+"%");
        System.out.println("Huellenzustand:                "+getHuelleInProzent()+"%");
        System.out.println("Lebenserhaltungssystemzustand: "+getLebenserhlatungssystemeInProzent()+"%");
        System.out.println("Androidenanzahl:               "+getAndroidenAnzahl());
        System.out.println("Schiffsname:                   "+getSchiffsname());
        System.out.println("-----------------------------------");
    }

    //Output of an Arraylist which collects the loads of the spaceship
    public void ladungsverzeichnisAusgeben() {
        int i = 0;
        int l = 1;
        if (ladungsverzeichnis.size() == 0)
            System.out.println("\n"+"Ladungsverzeichnis leer...");
        else {
            while (i < ladungsverzeichnis.size()) {
                System.out.println("\n"+"Ladung " + l + ": " + ladungsverzeichnis.get(i).getBezeichnung() + "\n  Menge: " + ladungsverzeichnis.get(i).getMenge());
                i++;
                l++;
            }
        }
    }

    //Clears the Arraylist if the amount of the certain load is zero
    public void ladungsverzeichnisAufrauemen(){
        int i = 0;
        ArrayList<Ladung> toDelete = new ArrayList<>();
        System.out.println("Es werden nur Ladung mit Menge = 0 entfernt!");
        while(i<ladungsverzeichnis.size()){
            if (ladungsverzeichnis.get(i).getMenge()==0){
                toDelete.add(ladungsverzeichnis.get(i));
            }
            i++;
        }
        ladungsverzeichnis.removeAll(toDelete);

        }
    }








