/**
 * Diese Klasse implementiert die Funktion die erstellten Raumschiffe in @see Raumschiff.java mit einer Ladung
 * auszustatten, welche in das Raumschiff-Objekt überladen werden kann.
 *
 * Dieser Code wurde im Auftrag für die Consulting GmbH geschrieben.
 *
 * @author Oskar Friedel
 * @version 0.1
 * @since 16.04.2021
 */
package StarTrek;

public class Ladung {

    private String bezeichnung;
    private int menge;

    //-----------------------------------------------------------------------------------------------------------------

    public Ladung (String bezeichnung, int menge){
        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }
    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public int getMenge() {
        return menge;
    }
    public void setMenge(int menge) {
        this.menge = menge;
    }

}
