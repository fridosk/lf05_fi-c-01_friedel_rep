package Angestellter;

public class Angestellter{

	private String vorname;
	private String name;
	private double gehalt;
	//TODO: 3. Fuegen Sie in der Klasse 'Angestellter' das Attribut 'vorname' hinzu und implementieren Sie die entsprechenden get- und set-Methoden.
	//TODO: 4. Implementieren Sie einen Konstruktor, der alle Attribute initialisiert.
	//TODO: 5. Implementieren Sie einen Konstruktor, der den Namen und den Vornamen initialisiert.

	public Angestellter(String vorname, String name, double gehalt)
	{
		this.vorname = vorname;
		this.name = name;
		this.gehalt = gehalt;
	}

	public Angestellter(String vorname, String name)
	{
		this.vorname = vorname;
		this.name = name;
	}

	public void setVorname(String vorname)
	{
		this.vorname = vorname;
	}

	public String getVorname()
	{
		return this.vorname;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setGehalt(double gehalt) {
		if (gehalt < 0)
			System.out.println("Gehalt nur �ber 0");
		else
			this.gehalt = gehalt;
		//TODO: 1. Implementieren Sie die entsprechende set-Methoden.
		//Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf.
	}

	public double getGehalt() {
		//TODO: 2. Implementieren Sie die entsprechende get-Methoden.
		return this.gehalt;
	}

	public String vollname() {

		return this.vorname + " " + this.name;
	}

	//TODO: 6. Implementieren Sie eine Methode 'vollname', die den vollen Namen (Vor- und Zuname) als string zur�ckgibt.
}
